# Github User Search

This is a React application that can search for users from github by leveraging the github api.

When we start typing the username in the search field the request is sent to the server to get the users with that name.
Deboucing is used while entering the text in the search field in order to reduce the number of requests to the server.

All the users cannot be loaded at once. So, Infinite scroll is implemented for loading more and more users on scroll end.
