import React, { useState } from 'react';
import { debounce } from 'lodash';
import SearchableSelect from '../searchable-select';
import './styles/index.scss';
import axios from 'axios';

const GITHUB_SEARCH_API = 'https://api.github.com/search/users';
const RESULTS_PER_PAGE = 20;

const Home = () => {
    const [searchResults, setSearchResults] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
	const [totalCount, setTotalCount] = useState(1);

    const onChange = debounce(event => {
        let searchText = event.target.value;
        if(searchText) {
            getUsers(searchText);
        } else {
            setSearchResults([]);
            setCurrentPage(1);
            setTotalCount(1);
        }
    }, 500);

    const loadMoreUsers = searchText => {
        getMoreUsers(searchText, currentPage + 1);
        setCurrentPage(currentPage + 1);
    }

    const getUsers = async (searchText) => {
        let response = await axios.get(`${GITHUB_SEARCH_API}?q=${searchText}&per_page=${RESULTS_PER_PAGE}&page=1`);
        let users = response.data.items;
        setTotalCount(response.data.total_count);
        setSearchResults([...users]);
    }

	const getMoreUsers = async (searchText, page) => {
        if(searchResults.length < totalCount) {
            let response = await axios.get(`${GITHUB_SEARCH_API}?q=${searchText}&per_page=${RESULTS_PER_PAGE}&page=${page}`);
            let users = response.data.items;
			console.log([...searchResults, ...users])
            setSearchResults([...searchResults, ...users]);
        }
    }

    return (<div className="container">
        <SearchableSelect onChange={onChange} items={searchResults} loadMore={loadMoreUsers} placeholder="Search user in github"/>
    </div>)
}

export default Home;