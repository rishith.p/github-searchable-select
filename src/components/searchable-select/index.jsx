import debounce from "lodash.debounce";
import React, { useRef } from "react";

import "./styles/styles";

const SearchableSelect = ({ items, onChange, loadMore, placeholder }) => {
    let searchResultsRef = useRef();
    let searchBarRef = useRef();

    const onScroll = debounce(() => {
        if (
            window.innerHeight + document.documentElement.scrollTop ===
            document.documentElement.offsetHeight
        ) {
            loadMore(searchBarRef.current.value);
        }
    }, 100);

    const renderResultItem = (item) => (
        <li className="result-item">
            <img src={item.avatar_url} className="avataar" />
            <div className="info-container">
                <span className="username">{item.login}</span>
                <span className="information">information</span>
            </div>
        </li>
    );

    return (
        <div className="searchable-select-container">
            <input
                type="text"
                className="search-box"
                ref={searchBarRef}
                onChange={onChange}
                placeholder={placeholder}
            />
            {items && items.length > 0 && <ul
                className="search-results"
                id="search-results"
                ref={searchResultsRef}
                onScroll={onScroll}
            >
                {React.Children.toArray(items.map(renderResultItem))}
            </ul>}
        </div>
    );
};

export default SearchableSelect;
